package ru.inno.library.services.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.inno.library.models.Account;
import ru.inno.library.models.Library;
import ru.inno.library.repositories.AccountsRepository;
import ru.inno.library.repositories.BooksRepository;
import ru.inno.library.repositories.LibrariesRepository;

import java.time.LocalTime;
import java.util.*;

import static jdk.internal.org.objectweb.asm.util.CheckClassAdapter.verify;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class LibrariesServiceImplTest {
    private static final long EXISTED_LIBRARY_ID = 1L;
    private static final long ACCOUNT_ID = 4L;
    private static final long NOT_EXISTED_LIBRARY_ID = 2L;
    private static final List<Account> ACCOUNTS_OF_EXISTED_LIBRARY = Arrays.asList(
            Account.builder().id(1L).email("test@1.com").build(),
            Account.builder().id(2L).email("test@2.com").build(),
            Account.builder().id(3L).email("test@3.com").build());

    private static final Account ACCOUNT = Account.builder()
            .id(ACCOUNT_ID)
            .email("test@4.com")
            .libraries(new HashSet<>())
            .build();
    private static final Library EXISTED_LIBRARY = Library.builder()
            .id(EXISTED_LIBRARY_ID)
            .name("Тестовая библиотека")
            .open(LocalTime.of(10, 11))
            .close(LocalTime.of(20, 00))

            .city("Город")
            .build();
    private LibrariesServiceImpl librariesService;
    private LibrariesRepository librariesRepository;
    private AccountsRepository accountsRepository;
    private BooksRepository booksRepository;

    @BeforeEach
    void setUp() {

        setUpMocks();

        stubMoks();

        this.librariesService = new LibrariesServiceImpl(librariesRepository, accountsRepository, booksRepository);
    }

    private void stubMoks() {
        when(librariesRepository.findById(EXISTED_LIBRARY_ID)).thenReturn(Optional.of(EXISTED_LIBRARY));
        when(librariesRepository.findById(NOT_EXISTED_LIBRARY_ID)).thenReturn(Optional.empty());

        when(accountsRepository.findAllByLibrariesContains(EXISTED_LIBRARY)).thenReturn(ACCOUNTS_OF_EXISTED_LIBRARY);
        when(accountsRepository.findById(ACCOUNT_ID)).thenReturn(Optional.of(ACCOUNT));
    }

    private void setUpMocks() {
        this.accountsRepository = Mockito.mock(AccountsRepository.class);
        this.librariesRepository = Mockito.mock(LibrariesRepository.class);
        this.booksRepository = Mockito.mock(BooksRepository.class);
    }



    @Test
    void add_account_to_library() {
        librariesService.addAccountToLibrary(1L,4L);
        Mockito.verify(accountsRepository).save(ACCOUNT);
    }


    @Test
    void get_in_library_accounts_for_existed_library() {
        List<Account> actual = librariesService.getInLibraryAccounts(EXISTED_LIBRARY_ID);
        List<Account> expected = ACCOUNTS_OF_EXISTED_LIBRARY;
        assertEquals(expected, actual);
    }
    @Test
    void get_in_library_accounts_for_not_existed_library() {
        assertThrows(RuntimeException.class, () -> {
            librariesService.getInLibraryAccounts(NOT_EXISTED_LIBRARY_ID);
        });
    }

}