package ru.inno.library.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookForm {

    private String name;
    private String author;
    private String description;
    private Integer edition;
}
