package ru.inno.library.services;

import ru.inno.library.dto.LibraryForm;
import ru.inno.library.models.Account;
import ru.inno.library.models.Book;
import ru.inno.library.models.Library;

import java.util.List;

public interface LibrariesService {

    List<Library> getAllLibraries();

    void addLibrary(LibraryForm library);

    Library getLibrary(Long id);

    void updateLibrary(Long libraryiId, LibraryForm library);

    void deleteLibrary(Long libraryId);

    void addAccountToLibrary(Long libraryId, Long accountId);

    List<Account> getNotInLibraryAccounts(Long libraryId);

    List<Account> getInLibraryAccounts(Long libraryId);

    List<Book> getNotInLibraryBooks(Long libraryId);


    List<Book> getInLibraryBooks(Long libraryId);

    void addBookToLibrary(Long libraryId, Long bookId);


}
