package ru.inno.library.services;

import ru.inno.library.dto.AccountForm;
import ru.inno.library.models.Account;
import ru.inno.library.models.Book;
import ru.inno.library.models.Library;
import ru.inno.library.security.details.CustomUserDetails;

import java.util.List;

public interface AccountsService {

    List<Account> getAllAccounts();

    void addAccount(AccountForm account);

    Account getAccount(Long id);

    void updateAccount (Long accountId, AccountForm account);

    void deleteAccount (Long accountId);

    List<Library> getNotInLAccountLibraries(Long accountId);

    List<Library> getInAccountLibraries(Long accountId);

    List<Book> getNotInAccountBooks(Long accountId);


    List<Book> getInAccountBooks(Long accountId);
    void returnBookToAccount(Long accountId, Long bookId);


    void addLibraryToAccount(Long accountId, Long libraryId);

    void addBookToAccount(Long accountId, Long bookId);

}
