package ru.inno.library.services.impl;

import lombok.*;
import org.springframework.stereotype.Service;
import ru.inno.library.dto.BookForm;
import ru.inno.library.models.Book;
import ru.inno.library.repositories.BooksRepository;
import ru.inno.library.services.BooksService;

import java.util.List;
@Service
@RequiredArgsConstructor
public class BooksServiceImpl implements BooksService {
    private final BooksRepository booksRepository;
    @Override
    public List<Book> getAllBooks() {
        return booksRepository.findAllByStateNot(Book.State.DELETED);
    }
    @Override
    public void addBook(BookForm book) {
        Book newBook = Book.builder()
                .name(book.getName())
                .author(book.getAuthor())
                .edition(book.getEdition())
                .description(book.getDescription())
                .state(Book.State.FREE)
                .build();
        booksRepository.save(newBook);

    }

    @Override
    public Book getBook(Long id) {
        return  booksRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateBook(Long bookId, BookForm updateDataBook) {
        Book bookForUpdate = booksRepository.findById(bookId).orElseThrow();

        bookForUpdate.setName(updateDataBook.getName());
        bookForUpdate.setAuthor(updateDataBook.getAuthor());
        bookForUpdate.setEdition(updateDataBook.getEdition());
        bookForUpdate.setDescription(updateDataBook.getDescription());

        booksRepository.save(bookForUpdate);

    }

    @Override
    public void deleteBook(Long bookId) {
        Book bookForDelete = booksRepository.findById(bookId).orElseThrow();
        bookForDelete.setState(Book.State.DELETED);

        booksRepository.save(bookForDelete);

    }

    @Override
    public void busyBook(Long bookId) {
        Book bookForBusy = booksRepository.findById(bookId).orElseThrow();
        bookForBusy.setState(Book.State.BUSY);

        booksRepository.save(bookForBusy);
    }

    @Override
    public void returnBook(Long bookId) {
        Book bookForReturn = booksRepository.findById(bookId).orElseThrow();
        bookForReturn.setState(Book.State.FREE);

        booksRepository.save(bookForReturn);

    }


}
