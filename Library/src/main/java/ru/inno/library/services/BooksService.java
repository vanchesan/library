package ru.inno.library.services;

import ru.inno.library.dto.AccountForm;
import ru.inno.library.dto.BookForm;
import ru.inno.library.models.Book;

import java.util.List;

public interface BooksService {
    List<Book> getAllBooks();

    Book getBook(Long id);

    void updateBook(Long bookId, BookForm book);

    void deleteBook(Long bookId);

    void busyBook(Long bookId);

    void returnBook(Long bookId);


    void addBook(BookForm book);


}
