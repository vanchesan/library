package ru.inno.library.services;

import ru.inno.library.dto.AccountForm;

public interface SignUpService {
    void signUp(AccountForm accountForm);
}
