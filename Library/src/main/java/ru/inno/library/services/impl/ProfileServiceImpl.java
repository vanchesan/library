package ru.inno.library.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.library.models.Account;
import ru.inno.library.repositories.AccountsRepository;
import ru.inno.library.repositories.BooksRepository;
import ru.inno.library.security.details.CustomUserDetails;
import ru.inno.library.services.ProfileService;

@RequiredArgsConstructor
@Service
public class ProfileServiceImpl implements ProfileService {

    private final AccountsRepository usersRepository;
    private final BooksRepository booksRepository;

    @Override
    public Account getCurrent(CustomUserDetails userDetails) {
        return usersRepository.findById(userDetails.getUser().getId()).orElseThrow();
    }


}
