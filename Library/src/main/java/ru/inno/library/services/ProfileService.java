package ru.inno.library.services;

import ru.inno.library.models.Account;
import ru.inno.library.security.details.CustomUserDetails;

public interface ProfileService {
    Account getCurrent(CustomUserDetails userDetails);

}
