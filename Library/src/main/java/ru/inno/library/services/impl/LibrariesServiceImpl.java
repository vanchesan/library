package ru.inno.library.services.impl;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.library.dto.LibraryForm;
import ru.inno.library.models.Account;
import ru.inno.library.models.Book;
import ru.inno.library.models.Library;
import ru.inno.library.repositories.AccountsRepository;
import ru.inno.library.repositories.BooksRepository;
import ru.inno.library.repositories.LibrariesRepository;
import ru.inno.library.services.LibrariesService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LibrariesServiceImpl implements LibrariesService {

    private final LibrariesRepository librariesRepository;

    private final AccountsRepository accountsRepository;


    private final BooksRepository booksRepository;

    @Override
    public List<Library> getAllLibraries() {
        return librariesRepository.findAllByStateNot(Library.State.DELETED);
    }

    @Override
    public void addLibrary(LibraryForm library) {
        Library newLibrary = Library.builder()
                .name(library.getName())
                .open(library.getOpen()).close(library.getClose())
                .city(library.getCity())
                .street(library.getCity())
                .house(library.getHouse())
                .description(library.getDescription())
                .state(Library.State.WORK)
                .build();
        librariesRepository.save(newLibrary);

    }

    @Override
    public Library getLibrary(Long id) {
        return librariesRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateLibrary(Long libraryId, LibraryForm updateData) {
        Library libraryForUpdate = librariesRepository.findById(libraryId).orElseThrow();

        libraryForUpdate.setName(updateData.getName());
        libraryForUpdate.setCity(updateData.getCity());
        libraryForUpdate.setOpen(updateData.getOpen());
        libraryForUpdate.setClose(updateData.getClose());
        libraryForUpdate.setStreet(updateData.getStreet());
        libraryForUpdate.setHouse(updateData.getHouse());
        libraryForUpdate.setStreet(updateData.getDescription());
        librariesRepository.save(libraryForUpdate);

    }

    @Override
    public void deleteLibrary(Long libraryId) {
        Library libraryForDelete = librariesRepository.findById(libraryId).orElseThrow();
        libraryForDelete.setState(Library.State.DELETED);

        librariesRepository.save(libraryForDelete);

    }

    @Override
    public void addAccountToLibrary(Long libraryId, Long accountId) {
        Library library = librariesRepository.findById(libraryId).orElseThrow();
        Account user = accountsRepository.findById(accountId).orElseThrow();

        user.getLibraries().add(library);

        accountsRepository.save(user);

    }

    @Override
    public List<Account> getNotInLibraryAccounts(Long libraryId) {

        Library library = librariesRepository.findById(libraryId).orElseThrow();
        return accountsRepository.findAllByLibrariesNotContainsAndState(library, Account.State.CONFIRMED);
    }

    @Override
    public List<Book> getNotInLibraryBooks(Long libraryId) {
        Library library = librariesRepository.findById(libraryId).orElseThrow();
        return booksRepository.findAllByLibrariesNotContainsAndState(library, Book.State.FREE);

    }

    @Override
    public List<Account> getInLibraryAccounts(Long libraryId) {
        Library library = librariesRepository.findById(libraryId).orElseThrow();
        return accountsRepository.findAllByLibrariesContains(library);
    }

    @Override
    public List<Book> getInLibraryBooks(Long libraryId) {
        Library library = librariesRepository.findById(libraryId).orElseThrow();
        return booksRepository.findAllByLibrariesContains(library);

    }

    @Override
    public void addBookToLibrary(Long libraryId, Long bookId) {
        Library library = librariesRepository.findById(libraryId).orElseThrow();
        Book book = booksRepository.findById(bookId).orElseThrow();


        book.getLibraries().add(library);

        booksRepository.save(book);

    }


}
