package ru.inno.library.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.library.dto.AccountForm;
import ru.inno.library.models.Account;
import ru.inno.library.models.Book;
import ru.inno.library.models.Library;
import ru.inno.library.repositories.AccountsRepository;
import ru.inno.library.repositories.BooksRepository;
import ru.inno.library.repositories.LibrariesRepository;
import ru.inno.library.services.AccountsService;

import java.util.List;

@Service
@RequiredArgsConstructor

public class AccountsServiceImpl implements AccountsService {

    private final AccountsRepository accountsRepository;
    private final LibrariesRepository librariesRepository;
    private final BooksRepository booksRepository;

    @Override
    public List<Account> getAllAccounts() {
        return accountsRepository.findAllByStateNot(Account.State.DELETED);
    }

    @Override
    public void addAccount(AccountForm account) {
        Account newAccount = Account.builder()
                .email(account.getEmail())
                .firstName(account.getFirstName())
                .lastName(account.getLastName())
                .state(Account.State.NOT_CONFIRMED)
                .build();
        accountsRepository.save(newAccount);
    }

    @Override
    public Account getAccount(Long id) {
        return accountsRepository.findById(id).orElseThrow();

    }

    @Override
    public void updateAccount(Long accountId, AccountForm updateAccount) {
        Account accountForUpdate = accountsRepository.findById(accountId).orElseThrow();
        accountForUpdate.setFirstName(updateAccount.getFirstName());
        accountForUpdate.setLastName(updateAccount.getLastName());
        accountForUpdate.setAge(updateAccount.getAge());

        accountsRepository.save(accountForUpdate);
    }

    @Override
    public void deleteAccount(Long accountId) {
        Account accountForDelete = accountsRepository.findById(accountId).orElseThrow();
        accountForDelete.setState(Account.State.DELETED);

        accountsRepository.save(accountForDelete);
    }

    @Override
    public List<Library> getNotInLAccountLibraries(Long accountId) {
        Account account = accountsRepository.findById(accountId).orElseThrow();
        return librariesRepository.findAllByAccountsNotContainsAndState(account, Library.State.WORK);
    }
    @Override
    public void addLibraryToAccount(Long accountId, Long libraryId) {
        Account account = accountsRepository.findById(accountId).orElseThrow();
        Library library = librariesRepository.findById(libraryId).orElseThrow();

        library.getAccounts().add(account);

        librariesRepository.save(library);
    }


    @Override
    public List<Library> getInAccountLibraries(Long accountId) {
        Account account = accountsRepository.findById(accountId).orElseThrow();
        return librariesRepository.findAllByAccountsContains(account);
    }

    @Override
    public List<Book> getNotInAccountBooks(Long accountId) {
        Account account = accountsRepository.findById(accountId).orElseThrow();
        return booksRepository.findAllByAccountsContains(account);
    }

    @Override
    public List<Book> getInAccountBooks(Long accountId) {
        Account account = accountsRepository.findById(accountId).orElseThrow();
        return booksRepository.findAllByAccountsNotContainsAndState(account, Book.State.FREE);
    }


    @Override
    public void returnBookToAccount(Long accountId, Long bookId) {
        Account account= accountsRepository.findById(accountId).orElseThrow();
        Book bookForReturn = booksRepository.findById(bookId).orElseThrow();
        bookForReturn.setState(Book.State.FREE);

        bookForReturn.getAccounts().add(account);

        booksRepository.save(bookForReturn);

    }




    @Override
    public void addBookToAccount(Long accountId, Long bookId) {
        Account account= accountsRepository.findById(accountId).orElseThrow();
        Book book = booksRepository.findById(bookId).orElseThrow();

        book.getAccounts().add(account);

        booksRepository.save(book);

    }

}
