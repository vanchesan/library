package ru.inno.library.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.inno.library.dto.AccountForm;
import ru.inno.library.models.Account;
import ru.inno.library.repositories.AccountsRepository;
import ru.inno.library.services.SignUpService;

@RequiredArgsConstructor
@Service
public class SignUpServiceImpl implements SignUpService {

    private final AccountsRepository usersRepository;

    private final PasswordEncoder passwordEncoder;

    @Override
    public void signUp(AccountForm accountForm) {
        Account newAccount = Account.builder()
                .firstName(accountForm.getFirstName())
                .lastName(accountForm.getLastName())
                .email(accountForm.getEmail())
                .password(passwordEncoder.encode(accountForm.getPassword()))
                .state(Account.State.CONFIRMED)
                .role(Account.Role.USER)
                .age(0)
                .build();

        usersRepository.save(newAccount);
    }
}
