package ru.inno.library.models;

import lombok.*;
import org.hibernate.annotations.Check;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = {"libraries", "accounts"})
@ToString(exclude = {"libraries", "accounts"})
@Table(name = "book")
public class Book {


    public enum State {
        FREE, BUSY, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true, nullable = false)
    private String name;

    @Column(unique = true, nullable = false)
    private String author;

    @Column
    @Check(constraints = "edition >=0")
    private Integer edition = 0;

    @Column
    private String description;

    @Enumerated(value = EnumType.STRING)
    private State state;

    @ManyToMany
    @JoinTable(joinColumns = {@JoinColumn(name = "book_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "library_id", referencedColumnName = "id")})
    private Set<Library> libraries;


    @ManyToMany
    @JoinTable(joinColumns = {@JoinColumn(name = "book_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "account_id", referencedColumnName = "id")})
    private Set<Account> accounts;


}
