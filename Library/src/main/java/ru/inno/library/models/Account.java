package ru.inno.library.models;

import lombok.*;
import org.hibernate.annotations.Check;

import javax.persistence.*;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"libraries", "books"})
@ToString(exclude = {"libraries", "books"})
@Builder
@Entity
@Table(name = "account")
public class Account {
    public enum State {
        NOT_CONFIRMED, CONFIRMED, DELETED
    }

    public enum Role {
        USER, ADMIN, LIBRARIAN
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String email;

    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column
    @Check(constraints = "age >= 0 and age <= 120")
    private Integer age = 0;


    @ManyToMany
    @JoinTable(joinColumns = {@JoinColumn(name = "account_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "library_id", referencedColumnName = "id")})
    private Set<Library> libraries;
    @ManyToMany
    @JoinTable(joinColumns = {@JoinColumn(name = "account_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "book_id", referencedColumnName = "id")})
    private Set<Book> books;


    @Enumerated(value = EnumType.STRING)
    private State state;

    @Enumerated(value = EnumType.STRING)
    private Role role;




}
