package ru.inno.library.models;

import lombok.*;
import org.hibernate.annotations.Check;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"books", "accounts"})
@ToString(exclude = {"libraries"})
@Entity
@Table(name = "library")
public class Library {

    public enum State {
        WORK, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", unique = true, nullable = false)
    private String name;
    @Column
    private LocalTime open;
    private LocalTime close;
    @Column
    private String city;


    @Column
    @Check(constraints = "house >= 0")
    private Integer house = 1;


    @Column(name = "description")
    private String description = "Место для описания";


    @Column(length = 1000)
    private String street = "улица";


    @Enumerated(value = EnumType.STRING)
    private State state;

    @ManyToMany(mappedBy = "libraries", fetch = FetchType.EAGER)
    private Set<Book> books;

    @ManyToMany(mappedBy = "libraries", fetch = FetchType.EAGER)
    private Set<Account> accounts;


}
