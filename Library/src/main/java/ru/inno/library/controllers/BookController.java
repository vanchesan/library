package ru.inno.library.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.library.dto.BookForm;
import ru.inno.library.security.details.CustomUserDetails;
import ru.inno.library.services.BooksService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/books")
public class BookController {

    private final BooksService booksService;

    @GetMapping
    public String getBooksPage(@RequestParam(value = "orderBy", required = false) String orderBy,
                               @RequestParam(value = "dir", required = false) String direction,
                               @AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("books", booksService.getAllBooks());
        return "books/books_page";
    }

    @PostMapping
    public String addBook(BookForm book) {
        booksService.addBook(book);
        return "redirect:/books";
    }

    @GetMapping("/{book-id}")
    public String getBookPage(@PathVariable("book-id") Long id,
                              @AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("book", booksService.getBook(id));
        return "books/book_page";
    }

    @PostMapping("/{book-id}/update")
    public String updateBook(@PathVariable("book-id") Long bookId, BookForm book) {
        booksService.updateBook(bookId, book);
        return "redirect:/books/" + bookId;
    }

    @GetMapping("/{book-id}/delete")
    public String updateBook(@PathVariable("book-id") Long bookId) {
        booksService.deleteBook(bookId);
        return "redirect:/books/";
    }

    @GetMapping("/{book-id}/busy")
    public String busyBook(@PathVariable("book-id") Long bookId) {
        booksService.busyBook(bookId);
        return "redirect:/books/";
    }

    @GetMapping("/{book-id}/return")
    public String returnBook(@PathVariable("book-id") Long bookId) {
        booksService.returnBook(bookId);
        return "redirect:/books/";
    }
}
