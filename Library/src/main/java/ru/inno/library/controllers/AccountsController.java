package ru.inno.library.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.library.dto.AccountForm;
import ru.inno.library.security.details.CustomUserDetails;
import ru.inno.library.services.AccountsService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/accounts")
public class AccountsController {

    private final AccountsService accountsService;

    @GetMapping
    public String getAccountPage(@RequestParam(value = "orderBy", required = false) String orderBy,
                                 @RequestParam(value = "dir", required = false) String direction,
                                 @AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("accounts", accountsService.getAllAccounts());
        return "accounts/accounts_page";
    }

    @PostMapping
    public String addAccount(AccountForm account) {
        accountsService.addAccount(account);
        return "redirect:/accounts";
    }

    @GetMapping("/{account-id}")
    public String getAccountPage(@PathVariable("account-id") Long accountId, Model model) {
        model.addAttribute("account", accountsService.getAccount(accountId));
        model.addAttribute("inAccountLibraries", accountsService.getInAccountLibraries(accountId));
        model.addAttribute("notInAccountLibraries", accountsService.getInAccountLibraries(accountId));
        model.addAttribute("notInAccountBooks", accountsService.getNotInAccountBooks(accountId));
        model.addAttribute("inAccountBooks", accountsService.getInAccountBooks(accountId));
        return "accounts/account_page";
    }

    @PostMapping("/{account-id}/update")
    public String updateAccount(@PathVariable("account-id") Long accountId, AccountForm account) {
        accountsService.updateAccount(accountId, account);
        return "redirect:/accounts/" + accountId;
    }

    @GetMapping("/{account-id}/delete")
    public String updateAccount(@PathVariable("account-id") Long accountId) {
        accountsService.deleteAccount(accountId);
        return "redirect:/accounts/";
    }

    @PostMapping("/{account-id}/libraries")
    public String addLibraryToAccount(@PathVariable("account-id") Long accountId,
                                      @RequestParam("library-id") Long libraryId) {
        accountsService.addLibraryToAccount(accountId, libraryId);
        return "redirect:/accounts/" + libraryId;

    }

    @PostMapping("/{account-id}/books")
    public String addBookToAccount(@PathVariable("account-id") Long accountId,
                                   @RequestParam("book-id") Long bookId) {
        accountsService.addBookToAccount(accountId, bookId);
        return "redirect:/accounts/" + accountId;
    }

//    @PostMapping("/{account-id}/books")
//    public String returnBookToAccount(@PathVariable("account-id") Long accountId,
//                                   @RequestParam("book-id") Long bookId) {
//        accountsService.returnBookToAccount (accountId, bookId);
//        return "redirect:/accounts/" + accountId;
//    }


}
