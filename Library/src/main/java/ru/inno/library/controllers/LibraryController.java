package ru.inno.library.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.library.dto.LibraryForm;
import ru.inno.library.security.details.CustomUserDetails;
import ru.inno.library.services.LibrariesService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/libraries")
public class LibraryController {

    private  final LibrariesService librariesService;

    @GetMapping
    public String getLibrariesPage(@RequestParam (value = "orderBy", required = false) String orderBy,
                                   @RequestParam (value = "dir", required = false) String direction,
                                   @AuthenticationPrincipal CustomUserDetails customUserDetails, Model model){
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("libraries", librariesService.getAllLibraries());
        return "libraries/libraries_page";
    }


    @PostMapping
    public String addLibrary(LibraryForm library){
        librariesService.addLibrary(library);
        return "redirect:/libraries";
    }
       @PostMapping("/libraries/{library-id}/update")
    public String updateLibrary(@PathVariable("library-id") Long libraryId, LibraryForm library) {
        librariesService.updateLibrary(libraryId, library);
        return "redirect:/libraries/" + libraryId;
    }

    @PostMapping("/{library-id}/accounts")
    public String addAccountToLibrary(@PathVariable("library-id") Long libraryId,
                                     @RequestParam("account-id") Long accountId) {
        librariesService.addAccountToLibrary(libraryId, accountId);
        return "redirect:/libraries/" + libraryId;
    }
    @PostMapping("/{library-id}/books")
    public String addBookToLibrary(@PathVariable("library-id") Long libraryId,
                                      @RequestParam("book-id") Long bookId) {
        librariesService.addBookToLibrary(libraryId, bookId);
        return "redirect:/libraries/" + libraryId;
    }

    @GetMapping("/{library-id}")
    public String getLibraryPage(@PathVariable("library-id") Long libraryId, Model model) {
        model.addAttribute("library", librariesService.getLibrary(libraryId));
        model.addAttribute("notInLibraryAccounts", librariesService.getNotInLibraryAccounts(libraryId));
        model.addAttribute("notInLibraryBooks", librariesService.getNotInLibraryBooks(libraryId));
        model.addAttribute("inLibraryBooks", librariesService.getInLibraryBooks(libraryId));
        model.addAttribute("inLibraryAccounts", librariesService.getInLibraryAccounts(libraryId));
        return "libraries/library_page";
    }

    @GetMapping("/{library-id}/delete")
    public String updateLibrary(@PathVariable("library-id") Long libraryId) {
        librariesService.deleteLibrary(libraryId);
        return "redirect:/libraries/";
    }

}
