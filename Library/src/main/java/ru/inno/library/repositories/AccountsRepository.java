package ru.inno.library.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.library.models.Account;
import ru.inno.library.models.Library;

import java.util.List;
import java.util.Optional;

public interface AccountsRepository extends JpaRepository<Account, Long> {
    List<Account> findAllByStateNot(Account.State deleted);

    List<Account> findAllByLibrariesNotContainsAndState(Library library, Account.State state);

    List<Account> findAllByLibrariesContains(Library library);

    Optional<Account> findByEmail(String email);
}
