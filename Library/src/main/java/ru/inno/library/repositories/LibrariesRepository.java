package ru.inno.library.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.library.models.Account;
import ru.inno.library.models.Library;

import java.util.List;

public interface LibrariesRepository extends JpaRepository<Library, Long> {
    List<Library> findAllByStateNot(Library.State deleted);

    List<Library> findAllByAccountsNotContainsAndState(Account account, Library.State confirmed);

    List<Library> findAllByAccountsContains(Account account);

}
