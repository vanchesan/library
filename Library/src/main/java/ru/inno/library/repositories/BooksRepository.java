package ru.inno.library.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.library.models.Account;
import ru.inno.library.models.Book;
import ru.inno.library.models.Library;

import java.util.List;

public interface BooksRepository extends JpaRepository<Book, Long> {
    List<Book> findAllByStateNot(Book.State deleted);

    List<Book> findAllByLibrariesNotContainsAndState(Library library, Book.State confirmed);

    List<Book> findAllByLibrariesContains(Library library);

    List<Book> findAllByAccountsNotContainsAndState(Account account, Book.State confirmed);

    List<Book> findAllByAccountsContains(Account account);


}
